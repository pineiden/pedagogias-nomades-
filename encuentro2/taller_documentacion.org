#+TITLE: Taller de Documentación con Software Libre
#+LATEX_HEADER: \usepackage[spanish]{babel}
#+LANGUAGE: es

* Descripción

Este taller está pensado para personas y organizaciones que necesiten
herramientas para generar documentos de manera sencilla y eficaz.

Dónde lo importante es *centrarse en el contenido* más que en la presentación,
que suele ser estándar.

Nos enfocaremos en aprender a escribir documentos en el sistema *org-mode* 
con el editor *Emacs*.

En realidad, los archivos son texto plano (como txt), pero que nosotros le
ponemos una extensión llamada *org*. Basta con esto para que el editor reconozca
los signos y nos permita distinguir títulos, listas, tablas y todo lo necesario.

Con este podremos escribir

- Actas

- Guías de clases

- Cuentos y poemas

- Artículos

- Gestionar proyectos 

- Calendario de actividades

- Presentaciones

Automáticamente genera los índices, reconoce secciones en diferentes niveles,
etctéra. Podemos agregar sencillamente imágenes, tablas, listas, citas al
contenido, enlaces.

* ¿Qué podemos obtener?

Una vez generado el contenido podemos crear distintas versiones en formatos
como:

- Gestión de proyectos con el mismo *org-mode*

- Documentos html (páginas web con información)

- Documentos pdf (documentos pdf)

- Documentos odt (documento writer)

- Documentos txt (archivos de texto)

- etctéra

Además, manejando la técnica de edición se podrán obtener mejoras en el tiempo 
de trabajo en la generación de estos documentos.

* ¿Qué tener instalado?

Además, hay configuraciones especiales que nos permiten generar presentaciones
rápidamente.

Será necesario tener instalado lo siguiente:

- GnuLinux (Debian, Ubuntu, CentOs, etc) 

- Emacs

- Texlive-full (para generar los pdf)


* ¿Aún con windows?

Se puede, pero no es tan bonito como con tecnologías libres.

Puedes usar *Atom* y el plugin *org-mode*

- Editor :: https://atom.io/

https://atom.io/packages/org-mode

* Otros sistemas operativos

https://www.gnu.org/software/emacs/download.html

* Tiempo de realización

- 1.5 horas :: Introducción a Org-mode

- 1.5 horas :: Org mode y características avanzadas

- 2 hrs :: Git y control de versiones

