#+TITLE: Introducción a la Documentación con Tecnologías Libres
#+AUTHOR: David Pineda Osorio
#+LATEX_HEADER: \usepackage[spanish]{babel}
#+LANGUAGE: es

* El Taller

Con el objetivo de entregar mejores herramientas para la creación de contenidos
a quienes desarrollan proyectos de enseñanza, pedagogías libres y nómadas es que
está dedicado este taller.

Es importante reconocer la necesidad de utilizar y crear documentos estándar,
que sean de acceso abierto y de disponibilidad comunitaria, que cada persona,
sin importar el dispositivo que tenga disponible, sin trabas económicas, pueda
acceder al conocimiento.

Por eso, es que este taller se basa en el uso de *tecnologías libres* para que
todas las personas puedan acceder y producir estos documentos.

** El contenido y la presentación de los documentos

En general, estamos acostumbrados a trabajar con tecnologías del capital, como
la suite de oficina *MS Office*, cuyo diseño está hecho para la acumulación de
capital.

Los documentos creados con esta tecnología tienen la dificultad de separar el
*contenido* de la *presentación*. Lo que hace que se nos sobrecargue de trabajo
al preocuparnos del formato, el cómo se ve y cosas que no son necesarias de
repetir cada vez.

Pensemos en que estamos creando un libro, con varios capítulos, necesitamos
enfocarnos netamente en el *contenido*, la presentación visual ya está definida,
por lo que no necesitamos perder nuestro tiempo en *repetir* cosas que ya hemos
hecho.

Por lo que aprenderemos a utilizar la interfaz para crear contenidos
consistentes y luego obtener los documentos en formato con la presentación
deseada.

** Ilustración del Contenido: Imágenes, diagramas, dibujos

Es de uso común colocar figuras que acompañen al contenido, ilustrándolo. Para
eso, no es necesario que dependamos de *herramientas privativas* pues hay
herramientas libres que cumplen a cabalidad la tarea de:

- procesar imágenes

- crear dibujos

- realizar diagramas 

Entre estas, destacan los *software libres* 

- Edición de fotografía :: Gimp

- Edición Vectorial :: Inkscape

Una parte del taller consiste en el uso básico de estos programas, de manera que
podamos diseñar y construir sin problemas las ilustraciones que acompañen
nuestro contenido.

** Control de versiones

Otro de los grandes problemas a los que se enfrentan las tecnologías privadas y
capitalistas es que son *muy complicadas* de gestionar las versiones, evolución
de los documentos y cohesión general (en caso de trabajar con varios documentos)

En conjunto con el sistema de escritura *org-mode* este taller trabaja con los
principios de *control de versiones* que utilizan los *programadores*, pues
trabajamos con *texto plano*, tal como ellos. Por lo que podremos contener un
proyecto, bien definido, sin problemas de versionado.


** Programa del Taller

Este es el programa del taller completo. Consiste en varias sesiones que pueden
ser realizadas durante una o dos jornadas, en total unas 12hrs.

*** Org Mode

Es el sistema de edición de textos base para crear documentos

[[file:./Org-mode-unicorn.svg.png]]

**** Introductorio (versión extendida)

- 3hrs :: Consiste en crear contenidos de un documento, tal como lo conocemos
            y exportar a algún formato que necesitemos. 

**** Avanzado

- 3hrs :: Consiste en ocupar algunas características avanzadas del sistema
            *org-mode*, en la que podremos, además de crear contenidos,
            gestionar proyectos, realizar cálculos con tablas, entre otros.

*** Ilustración

Consiste en aprender a utilizar las herramientas básicas de los programas para
la edición, creación y manipulación de imágenes, que además son *software libre*

**** Gimp: Manupulación de imágenes mapa de bits

- 2hrs :: 

Consiste en trabajar con imágenes (fotos, jpg, jpeg, png) y realizar algunas
transformaciones. 

- pasar a blanco y negro

- recortar

- componer imágenes

[[file:./gimp.jpg]]

**** Inkscape: Manipulación de imágenes vectoriales

- 2hrs ::

Consiste en trabajar en la realización de dibujos o diagramas vectoriales, que
ilustren alguna idea o concepto. Por ejemplo, la realización de:

- Un logo

- Un diagrama

- Un dibujo

- Operaciones de conjuntos

[[file:./inkscape]]

*** Control de Versiones con Git

- 2hrs :: 

Consiste en aprender a utilizar un programa para poder gestionar y planificar
correctamente el desarrollo de nuestro proyecto basado en documentos de texto.

[[file:./git.png]]

* ¿Por qué tecnologías libres?

Además, utilizando tecnologías libres podemos entrar a las *economías del común*
ya que son productos creados (en general) de manera voluntaria por la comunidad
de programadores y diseñadores (mujeres y hombres) alrededor del mundo, a través
del ciberespacio.

Significa, al tomar conciencia de la diferencia, de comenzar a participar de la
creación de las *nuevas cibernéticas* que se construyen desde lo *comunitario*.

Las tecnologías libres, nos ayudarán a construir y diseñar nuevas formas de
trabajo, basadas en lo colaborativo y lo colectivo. Considerando lo comunitario
como base, en que la riqueza lo es cuando fluye entre los comunes.

* Licencias de Derechos de Autor

Las licencias *Copyleft* son una forma de otorgar a las personas la posibilidad
de *enriquecer* el trabajo y contenidos ya que les da la posibilidad de acceder
a ellos, reutilizarlos y /modificarlos/.

Pero también, en otra reflexión es entrar en juego de lo /colonizado/. Si
decimos simplemente que toda producción cultural es un bien común de la
humanidad, ¿no será mejor?

Tal vez, utilizar una licencia de tipo *Copy far left* [[https://lynnesbian.space/csl/][Documento]], sea más
conveniente para nuestro caso.

Es interesante también la discusión que se da en torno al
"[[https://endefensadelsl.org/manifiesto_telecomunista.html][Manifiesto Telecomunista]]".

Por lo tanto, este trabajo cuenta con la siguiente licencia:

- Licencia de Producción entre Pares
  (https://endefensadelsl.org/ppl_deed_es.html)

Con esta puedes:

- Compartir ::  Copiar, distribuir, ejecutar y comunicar públicamente la obra

- Hacer Obras Derivadas :: Modificar la obra

Con la condición de:

- Atribuir

- Mantener la Misma Licencia (P2P)

- No Capitalista

En extenso:

[[https://endefensadelsl.org/ppl_deed_es.html][Licencia P2P]]

* Contactar a 

- Autor :: David Pineda Osorio

- Alias :: Pineiden

- Email :: dpineda@uchile.cl (es personal)

- Teléfono :: +56982142267
