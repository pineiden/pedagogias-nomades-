(TeX-add-style-hook
 "org_mode_basico"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem") ("babel" "spanish")))
   (add-to-list 'LaTeX-verbatim-environments-local "minted")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "inputenc"
    "fontenc"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref"
    "minted"
    "babel")
   (LaTeX-add-labels
    "sec:orge858340"
    "sec:org304a441"
    "sec:org38013d9"
    "sec:orgc0d25a6"
    "sec:org81f08b4"
    "sec:org5567595"
    "sec:org3a6c5db"
    "sec:org391aa26"
    "sec:orgb06f88d"
    "sec:org96636aa"
    "sec:org1cfb57f"
    "sec:orge7c6e36"
    "sec:orgf77debc"
    "sec:org306dbc4"
    "sec:org8716030"
    "sec:orgf94af8e"
    "sec:org92dbc59"
    "sec:orgd298059"
    "sec:org93ccef1"
    "sec:org0636caa"
    "sec:org5b1ae1b"
    "sec:org9d77aa3"
    "sec:org921c56e"
    "sec:org6a39fce"
    "sec:org08a20f6"
    "sec:orgc59eca7"
    "sec:orgebfe779"
    "sec:org180fb28"
    "sec:org7368ca4"
    "sec:org8b789a3"
    "sec:orgf66efae"
    "sec:org2ebad83"
    "sec:org670de0c"
    "sec:orgaadcad3"
    "sec:orgc650097"
    "sec:orgbe68224"
    "sec:orgc1f31d7"
    "sec:org0b09a4c"
    "sec:org25b666d"
    "sec:org9abaf62"
    "sec:org5630dc4"
    "sec:org08916a0"
    "sec:org56fc4a5"
    "sec:org7a082ad"
    "sec:orgb24776f"
    "sec:orgf49d527"
    "sec:org194b93f"
    "sec:orge07874a"
    "sec:orga6f7ea8"
    "sec:org35b7cd4"
    "sec:org1e0b862"
    "sec:org2cacd1b"
    "sec:org1a148c5"
    "fig:org95c49f4"
    "sec:org03d7f1f"
    "sec:orgf79e0ba"
    "sec:org760829d"
    "sec:orga308d78"
    "sec:orgf4d5ccc"
    "sec:org5a281d2"
    "sec:org8cf31bd"))
 :latex)

