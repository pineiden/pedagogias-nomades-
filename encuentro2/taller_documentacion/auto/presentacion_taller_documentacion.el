(TeX-add-style-hook
 "presentacion_taller_documentacion"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem") ("babel" "spanish")))
   (add-to-list 'LaTeX-verbatim-environments-local "minted")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "inputenc"
    "fontenc"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref"
    "minted"
    "babel")
   (LaTeX-add-labels
    "sec:orgfb18efd"
    "sec:org240c1ec"
    "sec:org503ed62"
    "sec:orga7d70af"
    "sec:org8601a3d"
    "sec:orgb2a9a0f"
    "sec:org7eadacc"
    "sec:org1e3ddf4"
    "sec:orga3d1ca6"
    "sec:org1130dc3"
    "sec:org83541bd"
    "sec:org9c04ad4"
    "sec:org8b98422"
    "sec:org3073248"
    "sec:org493ec02"))
 :latex)

